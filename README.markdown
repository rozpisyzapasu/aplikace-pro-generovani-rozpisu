# GENERÁTOR ROZPISŮ

* nástroj k vygenerování rozpisů sportovních turnajů a lig
* správa rozpisů - přidávání výsledku ze kterých se vypočte tabulka
* ukládejte XML soubor, jestliže chcete dále upravovat rozpis (vkládat výsledky)
* uložený HTML soubor můžete pouze otevřít ve vašem webovém prohlížeči a příp. vytisknout
* upozornění: aplikace nebude již nijak aktualizována (o playoff, turnaje)

## Co potřebujete, abyste mohli používat tuto aplikaci?

* **JRE**
* zkuste program stáhnout a uvidíte, jestli vám půjde (spuštění klasickým dvojklikem), když nepůjde, tak si musíte stáhnout JRE
* jestli nevíte, zda máte JAVU naistalovanou, tak se koukněte např. na web [www.presnycas.cz](http://www.presnycas.cz), když vám bude web fungovat bez problémů (bude zobrazovat čas), tak vám půjde bez problémů i tato aplikace

## O programu

* *Rok vytvoření*: 2011.
* *Autor*: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).
* *License*: [GNU GPL 3](https://bitbucket.org/sports-scheduler/aplikace-pro-generov-n-rozpis-cs/src/tip/LICENCE.txt).

## Poznámky ke zdrojovým kódům
 
* Program vytvořen jako semestrálka pro předmět [Programovací techniky v jazyce Java](http://ects.upce.cz/predmet/KIT/IJAV?lang=cs&rocnik=2) (UPCE).
* Další informace o programu a ukázkové video lze nalézt na [www.online-generator-rozpisu.g6.cz/aplikace/](http://www.online-generator-rozpisu.g6.cz/aplikace/)
* Jedná se o projekt pro **JDeveloper**
* Zdrojové soubory jsou ve složce **Semestralka/src**
* Pro práci s XML používá knihovnu [**JDOM**](http://www.jdom.org/)
    * měl jsem problém s připojením knihovny v JDeveloperu při exportu do jar (složka deploy), takže jsem to nakonec vyřešil tím, že jsem JDOM rozbalil a ručně vložil do složky po kompilaci (classes) 
    * v případě rebuildu projektu v JDeveloperu je nutno složku /org/jdom opět vložit do classes

## Licence

Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License 3, or any later version
For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.