<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes" method="html" encoding="utf-8"/>
  <!-- Root template -->
  <xsl:template match="/">
    
     <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tabulka</title>
        <style type="text/css">
        body {
          color:black;          
        }
        h1 {
          text-align:center;
          font-size:0.8em;
          color:#ccf;
        }
        h2 {
          font-size:1.2em;
          color:#006;
          margin:1ex 0;
        }
        table.tabulka {
            width:100%;
        }
        table.tabulka td {
            padding:3pt;
            border:0;
            width:10%;
            text-align: center;
            border-bottom:1px solid #6699ff;
        }
        table.tabulka td.skore {
            color:red;
            font-weight:bold;
        }
        table.tabulka tr.nadpis {
            color:white;
            background-color:#6699ff;
            font-size:9pt;
            text-transform: uppercase;
            font-style:italic
        }        
        table.tabulka tr.nadpis td{
            padding:5pt
        }
        table.tabulka td.sirsi {
            width:30%;
        }
        </style>
      </head>
      <body>
        <h1>www.online-generator-rozpisu.g6.cz</h1>      
        <h2>Tabulka</h2>
        <table class="tabulka" cellspacing="0">
         <tr class="nadpis">   
             <td>Pořadí</td>
             <td class="sirsi">Tým</td>
             <td>Zápasy</td>
             <td>Výhry</td>
             <td>Remízy</td>
             <td>Prohry</td>             
             <td>Skóre</td>
             <td>Body</td>           
         </tr>                 
         <xsl:for-each select="tabulka/tym">          
         <tr>
             <td><xsl:value-of select="@poradi" /></td>
             <td class="sirsi"><strong><xsl:value-of select="@nazev" /></strong></td>
             <td><xsl:value-of select="@zapasy" /></td>
             <td><xsl:value-of select="@vyhry" /></td>
             <td><xsl:value-of select="@remizy" /></td>
             <td><xsl:value-of select="@prohry" /></td>
             <td><xsl:value-of select="@vg" /> : <xsl:value-of select="@og" /></td>
             <td class="skore"><xsl:value-of select="@body" /></td>
         </tr>
         </xsl:for-each>                         
        </table>
      </body>
     </html>

  </xsl:template>
</xsl:stylesheet>
