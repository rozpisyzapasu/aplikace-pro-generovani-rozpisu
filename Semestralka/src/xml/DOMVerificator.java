package xml;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.io.IOException;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class DOMVerificator extends JComponent {
    
    private static final String XSD_SCHEMA =
      Export.class.getResource("schema_rozpis.xsd").toString();

    public static boolean overSouborXML(String soubor) {
        // KONTROLA VALIDACE POMOCI DOMU
        try {
            // obalka parseru - vypinam validaci, protoze validuji podle externiho schema
            DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            // nastaveni schema - externi schema.xsd
            SchemaFactory schemaFactory =
                SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            factory.setSchema(schemaFactory.newSchema(new Source[] { new StreamSource(XSD_SCHEMA) }));
            // vytvoreni parseru a error handleru
            DocumentBuilder builder = factory.newDocumentBuilder();          
            DOMErrorHandler handler = new DOMErrorHandler();
            builder.setErrorHandler(handler);
            builder.parse(new InputSource(soubor));
            // i kdyz bude chyba, tak pujde program dale, proto musim overit vyjimky u handleru            
            if (handler.error) {
                zobrazHlasku(handler.vyjimka, handler.typVyjimky);
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            zobrazHlasku(e, "IOException");
            e.printStackTrace();
            return false;
        } catch (SAXException e) {
            zobrazHlasku(e, "SAXException");
            return false;
        } catch (ParserConfigurationException e) {
            zobrazHlasku(e, "ParserConfigurationException");
            return false;
        }
    }

    private static void zobrazHlasku(Exception e, String error) {
        JOptionPane.showMessageDialog(null, e.getMessage(), error,
                                      JOptionPane.ERROR_MESSAGE);
    }

    private static class DOMErrorHandler implements ErrorHandler {
        private boolean error = false;
        private String typVyjimky = "";
        private SAXParseException vyjimka = null;

        public void warning(SAXParseException e) throws SAXException {
            error = true;
            typVyjimky = "VAROVÁNÍ";
            vyjimka = e;
        }

        public void error(SAXParseException e) throws SAXException {
            error = true;
            typVyjimky = "CHYBA";
            vyjimka = e;
        }

        public void fatalError(SAXParseException e) throws SAXException {
            error = true;
            typVyjimky = "ZÁVAŽNÁ CHYBA";
            vyjimka = e;
        }
    }
}
