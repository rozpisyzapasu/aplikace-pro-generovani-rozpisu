package xml;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Element;

public class Tabulka {

    private Map<String, Tym> tymySkore;

    public Tabulka(Object[] tymy) {
        this.tymySkore = new HashMap<String, Tym>(tymy.length);
        for (int i = 0; i < tymy.length; i++) {
            this.tymySkore.put(tymy[i].toString(), new Tym());
        }
    }

    public boolean zpracujZapas(Element zapas) {
        Element domaci = zapas.getChild("domaci");
        Element hoste = zapas.getChild("hoste");
        String golyD = domaci.getChildText("golyD");
        String golyH = hoste.getChildText("golyH");
        if (!golyD.isEmpty() && !golyH.isEmpty()) {
            int gD = Integer.parseInt(golyD);
            int gH = Integer.parseInt(golyH);
            String d = domaci.getChildText("jmenoD");
            String h = hoste.getChildText("jmenoH");
            zpracujVysledek(d, h, gD, gH);
            return true;
        }
        return false;
    }

    private void zpracujVysledek(String dom, String hos, int gD, int gH) {
        Tym domaci = tymySkore.get(dom);
        Tym hoste = tymySkore.get(hos);
        if (gD > gH) {
            domaci.zapisVyhru(gD, gH);
            hoste.zapisProhru(gH, gD);
        } else if (gD == gH) {
            domaci.zapisRemizu(gD, gH);
            hoste.zapisRemizu(gH, gD);
        } else {
            domaci.zapisProhru(gD, gH);
            hoste.zapisVyhru(gH, gD);
        }
    }

    public Object[][] getVysledkovaTabulka() {
        Object[][] pole = new Object[tymySkore.size()][9];
        Iterator<Map.Entry<String, Tym>> it = getIteratorSetrideneTabulky();
        int i = 0;
        while (it.hasNext()) {
            Map.Entry<String, Tym> hodnota = it.next();
            pole[i][0] = (i + 1) + ".";
            pole[i][1] = hodnota.getKey();
            Object[] skore = hodnota.getValue().toArray();
            System.arraycopy(skore, 0, pole[i], 2, skore.length);
            i++;
        }
        return pole;
    }

    private Iterator<Map.Entry<String, Tym>> getIteratorSetrideneTabulky() {
        List<Map.Entry<String, Tym>> list =
            new ArrayList<Map.Entry<String, Tym>>(tymySkore.entrySet());
        Collections.sort(list, new KomparatorTymu());
        return list.iterator();
    }

    /**
     * Vnitrni trida pro porovnani tymu. Vsechny setridene mapy tridi podle
     * klíčů a já potřebuji třídit podle hodnot
     *  Jednoduseji: jen pokud bych do Tymu pridal i jejich nazev, pak bych pouze
     *  vytahl values a ty setridil
     */
    private class KomparatorTymu implements Comparator<Map.Entry<String, Tym>> {
        public int compare(Map.Entry<String, Tym> t1,
                           Map.Entry<String, Tym> t2) {
            return t1.getValue().compareTo(t2.getValue());
        }
    }

    private class Tym implements Comparable<Tym> {
        private int zapasy;
        private int vyhry;
        private int prohry;
        private int remizy;
        private int vstreleneGoly;
        private int obdrzeneGoly;
        private int body;

        public Tym() {
            super();
        }

        /**
         *pretypovani na String, protoze kdyz je Integer (int), tak nefunguje
         * zarovnani v jtablu na stred
         * @return
         */
        private Object[] toArray() {
            return new Object[] { String.valueOf(zapasy),
                                  String.valueOf(vyhry),
                                  String.valueOf(remizy),
                                  String.valueOf(prohry),
                                  String.valueOf(vstreleneGoly),
                                  String.valueOf(obdrzeneGoly), 
                                  String.valueOf(body)};
        }

        private void zapisVyhru(int vg, int og) {
            zapasy++;
            vyhry++;
            vstreleneGoly += vg;
            obdrzeneGoly += og;
            body += 3;
        }

        private void zapisRemizu(int vg, int og) {
            zapasy++;
            remizy++;
            vstreleneGoly += vg;
            obdrzeneGoly += og;
            body += 1;
        }

        private void zapisProhru(int vg, int og) {
            zapasy++;
            prohry++;
            vstreleneGoly += vg;
            obdrzeneGoly += og;
        }

        public int compareTo(Tabulka.Tym o) {
            if (this.body < o.body) {
                return 1;
            } else if (this.body > o.body) {
                return -1;
            } else {
                // prvni porovnani podle poctu zapasu
                if (this.zapasy != o.zapasy) {
                    return (this.zapasy < o.zapasy) ? -1 : 1;
                    // 2. podle poctu vstrelenych golu
                } else if (this.vstreleneGoly != o.vstreleneGoly) {
                    return (this.vstreleneGoly > o.vstreleneGoly) ? -1 : 1;
                    // 3. podle poctu obdrzenych
                } else if (this.obdrzeneGoly != o.obdrzeneGoly) {
                    return (this.obdrzeneGoly < o.obdrzeneGoly) ? -1 : 1;
                    // 4. maji vsechno stejne
                } else {
                    return 0;
                }
            }
        }
    }
}
