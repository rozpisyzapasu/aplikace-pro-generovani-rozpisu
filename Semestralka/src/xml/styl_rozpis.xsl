<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes" method="html" encoding="utf-8"/>
  <!-- Root template -->
  <xsl:template match="/">     

     <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Rozpis</title>
        <style type="text/css">
        body {
          color:black;          
        }
        h1 {
          text-align:center;
          font-size:0.8em;
          color:#ccf;
        }
        h2 {
          font-size:1.2em;
          color:#006;
          margin:1ex 0;
        }
        table.rozpis {
            width:100%;
        }
        table.rozpis td {
            padding:2pt;
            border:0;
            width:40%;
            text-align: center;
        }
        table.rozpis td.perioda {
            color:white;
            background-color:#000;
            font-size:8pt;
            border:0;
        }
        table.rozpis td.kolo {
            color:white;
            background-color:#6699ff;
            font-size:9pt;
            text-transform: uppercase;
            border-top: 1px solid #6699ff;
            border-bottom: 1px solid #6699ff;
          }
          table.rozpis td.vysledek {
            color:red;
            width:15%;
            font-weight:bold;
            text-align: center;
          }
          table.info {
              border-top:1px solid #ccf;;
              width:100%;
          }
          table.info td {
              border-bottom:1px solid #ccf;;
              padding:2pt;              
              width:20%;
              text-align: left;
              font-size:0.9em;
              font-style:italic;
              color:#aaa;
          }
          table.info td.value {
              color:black;
              font-weight:700;
              font-style:normal;
              width:79%;           
          }          
        </style>
      </head>
      <body>
        <h1>www.online-generator-rozpisu.g6.cz</h1>      
        <h2>Informace o rozpisu</h2>
        <table class="info" cellspacing="0">
         <tr>
          <td>Počet period: </td>
          <td class="value"><xsl:value-of select="rozpis/informace/pocetPeriod" /></td>
         </tr>
         <tr>
          <td>Počet kol v jedné periodě: </td>
          <td class="value"><xsl:value-of select="rozpis/informace/pocetKolPeriody" /></td>
         </tr>
         <tr>
          <td>Počet zápasů v kole: </td>
          <td class="value"><xsl:value-of select="rozpis/informace/pocetZapasuKola" /></td>
         </tr>
         <tr>
          <td>Počet týmů: </td>
          <td class="value"><xsl:value-of select="rozpis/informace/tymy/@pocetTymu" /></td>
         </tr>
         <tr>
          <td>Týmy: </td>
          <td class="value">
           <xsl:for-each select="rozpis/informace/tymy/tym">          
             <xsl:value-of select="@jmeno" />
             <xsl:if test="position() != last()">
              <xsl:text>, </xsl:text>
             </xsl:if>
           </xsl:for-each>                
          </td>
         </tr>
        </table>
         
        <h2>Rozlosování</h2>
        <table class="rozpis" cellspacing="0">        
         <xsl:for-each select="rozpis/rozlosovani/perioda">          
          <tr>
           <td class="perioda" colspan="3"><xsl:value-of select="@cisloPeriody" />. perioda</td>
          </tr>
          <xsl:for-each select="kolo">          
            <tr>
             <td class="kolo" colspan="3"><xsl:value-of select="@cisloKola" />. kolo</td>
            </tr>
            <xsl:for-each select="zapas">          
              <xsl:if test="domaci != ''">
              <tr>
               <td><xsl:value-of select="domaci/jmenoD" /></td>
               <td class="vysledek"><xsl:value-of select="domaci/golyD" /> : <xsl:value-of select="hoste/golyH" /></td>
               <td><xsl:value-of select="hoste/jmenoH" /></td>
              </tr>
              </xsl:if>
            </xsl:for-each>        
          </xsl:for-each>        
         </xsl:for-each>        
        </table>        
      </body>
     </html>

  </xsl:template>
</xsl:stylesheet>
