package xml;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */


import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalAddException;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.transform.XSLTransformException;


public class RozpisXML {
    /*
     * pri ulozeni Stringu s "path" nefungovalo spojení na soubor, který měl
     * v cestě nějakou složku s diakritikou
     * - java.net.MalformedURLException: unknown protocol: d
     * RESENI:- ulozit zde File
     *        - predat String = file.toURI().getPath();
    */
    private String soubor;
    private Element koren;
    private Element aktualniKolo;

    /**
     * konstruktor po vygeneorávní sezony, která dodá koren
     */
    public RozpisXML(Element koren, String soubor) throws JDOMException,
                                                          IOException {
        this.koren = koren;
        this.soubor = soubor;
    }

    /**
     * konstruktor pro upravy jiz exitujiciho xml souboru
     */
    public RozpisXML(String soubor) throws JDOMException, IOException {
        this.soubor = soubor;
        this.nactiRozpisXML();
    }

    /**
     * Provede načtení xml souboru do paměti a následně pracuji pouze s kořenem
     * @throws JDOMException, IOException
     */
    private void nactiRozpisXML() throws JDOMException, IOException {
        SAXBuilder sb = new SAXBuilder();
        Document doc = sb.build(soubor.toString());
        koren = doc.getRootElement();
        doc.detachRootElement();
    }

    /**
     * @return absolutní adresa xml souboru (stejná jako původní při vytváření instance)
     */
    public String getXmlAdresa() {
        return soubor;
    }

    /**
     * @return absolutní adresa souboru, kde je nahrazena koncovla xml za html
     */
    public String getHtmlAdresa() {
        return getHtmlAdresa(".html");
    }

    public String getHtmlAdresaTabulky() {
        return getHtmlAdresa("-tabulka.html");
    }

    private String getHtmlAdresa(String nahrada) {
        return soubor.substring(0, soubor.length() - 4) + nahrada;
    }

    // GETTERY

    private Element getKoren() {
        // i kdyz jsem pouzil detache u dokumentu i rootu, tak mi stejne obcas skocila chyba
        return (Element)koren.clone();
    }

    public int getPocetPeriod() {
        return Integer.parseInt(koren.getChild("informace").getChildText("pocetPeriod"));
    }

    public int getPocetKolPeriody() {
        return Integer.parseInt(koren.getChild("informace").getChildText("pocetKolPeriody"));
    }

    public int getPocetZapasuKola() {
        return Integer.parseInt(koren.getChild("informace").getChildText("pocetZapasuKola"));
    }

    public int getPocetTymu() {
        return Integer.parseInt(koren.getChild("informace").getChild("tymy").getAttributeValue("pocetTymu"));
    }

    public int getCelkemZapasu() {
        return getPocetPeriod() * getPocetKolPeriody() * getPocetZapasuKola();
    }

    /**
     * @return setříděné (česky) pole týmů (třídí se už po generování)
     */
    public Object[] getTymy() {
        List<String> tymy = new ArrayList<String>(getPocetTymu());
        List<Element> listTymy =
            koren.getChild("informace").getChild("tymy").getChildren();
        for (int i = 0; i < listTymy.size(); i++) {
            tymy.add(i, listTymy.get(i).getAttributeValue("jmeno"));
        }
        return tymy.toArray();
    }

    /**
     * Nastavení aktuálního kola, ani nekontroluji meze, protože z puštěného
     * programu (GUI) nemůže přijít žádná špatná hodnota (díky comboboxu)
     */
    public void nastavAktualniKolo(int cisloPeriody, int cisloKola) {
        Element rozpis = koren.getChild("rozlosovani");
        Element perioda =
            (Element)rozpis.getChildren("perioda").get(cisloPeriody);
        aktualniKolo = (Element)perioda.getChildren("kolo").get(cisloKola);
    }

    /**
     * @return String[počet zápasů kola][4] (kde 0 = domácí, 1 = góly domácí, 2 = góly hosté, 3 = hosté)
     */
    public String[][] getZapasyAktKola() {
        String[][] data = new String[getPocetZapasuKola()][4];
        List<Element> zapasy = aktualniKolo.getChildren();
        for (int k = 0; k < zapasy.size(); k++) {
            Element domaci = zapasy.get(k).getChild("domaci");
            Element hoste = zapasy.get(k).getChild("hoste");
            data[k][0] = domaci.getChildText("jmenoD");
            data[k][1] = getVysledek(domaci.getChildText("golyD"));
            data[k][2] = getVysledek(hoste.getChildText("golyH"));
            data[k][3] = hoste.getChildText("jmenoH");
        }
        return data;
    }

    /**
     * v xml držím prázdný výsledek, ale v JTable zobrazuji pomlčku '-'
     */
    private String getVysledek(String goly) {
        return goly == null ? "-" : goly;
    }

    /**
     * @param cisloZapasu - aktuální řádek tabulky
     * @param tym - aktuální sloupec tabulky (1 = domaci, 2 = hoste)
     * @param goly - skóre vytažené z comboboxu
     */
    public void nastavVysledek(int cisloZapasu, int tym, String goly) {
        Element zapas = (Element)aktualniKolo.getChildren().get(cisloZapasu);
        if (tym == 1) {
            zapas.getChild("domaci").getChild("golyD").setText(goly);
        } else if (tym == 2) {
            zapas.getChild("hoste").getChild("golyH").setText(goly);
        }
    }

    /**
     * @return celý rozpis v podobě String[všechny zápasy + nadpisy][4]
     */
    public String[][] getRozpis() {
        int pocetRadku =
            getCelkemZapasu() + getPocetPeriod() * (1 + getPocetKolPeriody());
        return Export.rootToArray(getKoren(), pocetRadku);
    }

    public Exception exportXML() {
        try {
            Export.rootToXML(getKoren(), getXmlAdresa());
        } catch (FileNotFoundException f) {
            return f;
        } catch (IOException f) {
            return f;
        } catch (IllegalAddException f) {
            return f;
        } catch (XSLTransformException f) {
            return f;
        }
        return null;
    }

    public Exception exportHTML() {
        try {
            Export.rootToHTML(getKoren(), getHtmlAdresa(".html"));
        } catch (FileNotFoundException f) {
            return f;
        } catch (IOException f) {
            return f;
        } catch (IllegalAddException f) {
            return f;
        } catch (XSLTransformException f) {
            return f;
        }
        return null;
    }

    public Object[][] getVysledkovaTabulka() {
        return Export.rootToTableArray(getKoren(), getTymy());
    }

    public Exception exportTabulkaHTML() {
        try {
            Export.rootToTableHTML(getTabulkaKoren(),
                                   getHtmlAdresa("-tabulka.html"));
        } catch (FileNotFoundException f) {
            return f;
        } catch (IOException f) {
            return f;
        } catch (IllegalAddException f) {
            return f;
        } catch (XSLTransformException f) {
            return f;
        }
        return null;
    }

    // koren pro export do XML

    private Element getTabulkaKoren() {
        Object[][] tabulka = getVysledkovaTabulka();
        Element koren = new Element("tabulka");
        koren.setAttribute("pocetTymu", "" + tabulka.length);
        for (int i = 0; i < tabulka.length; i++) {
            Element tym = new Element("tym");
            tym.setAttribute("poradi", "" + tabulka[i][0]);
            tym.setAttribute("nazev", "" + tabulka[i][1]);
            tym.setAttribute("zapasy", "" + tabulka[i][2]);
            tym.setAttribute("vyhry", "" + tabulka[i][3]);
            tym.setAttribute("remizy", "" + tabulka[i][4]);
            tym.setAttribute("prohry", "" + tabulka[i][5]);
            tym.setAttribute("vg", "" + tabulka[i][6]);
            tym.setAttribute("og", "" + tabulka[i][7]);
            tym.setAttribute("body", "" + tabulka[i][8]);
            koren.addContent(tym);
        }
        return koren;
    }

    @Override
    public String toString() {
        return soubor + "(period=" + getPocetPeriod() + ", kol = " +
            getPocetKolPeriody() + ", zapasuKola = " + getPocetZapasuKola() +
            ", tymu = " + getPocetTymu() + ")";
    }
}
