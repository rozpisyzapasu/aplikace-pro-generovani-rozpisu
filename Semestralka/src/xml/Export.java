package xml;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.IllegalAddException;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.transform.XSLTransformException;
import org.jdom.transform.XSLTransformer;

public class Export {

    private static final String XML_ROZPIS =
        Export.class.getResource("styl_xml.xsl").toString();
    private static final String HTML_ROZPIS =
        Export.class.getResource("styl_rozpis.xsl").toString();
    private static final String HTML_TABULKA =
        Export.class.getResource("styl_tabulka.xsl").toString();

    /**
     * Exportování do XML s kořenem, který získám
     *  - z instance Sezony(po vygenerování), uloží do místa umístění jaru
     *  - z exist. xml souboru, uloží místo původního xml
     * @param koren1
     * @param soubor
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void rootToXML(Element koren,
                                 String soubor) throws FileNotFoundException,
                                                       IOException,
                                                       IllegalAddException,
                                                       XSLTransformException {
        docToFile(transform(koren, XML_ROZPIS), soubor);
    }

    /**
     * Exportování do HTML s kořenem, který získám
     *  - z instance Sezony (po vygenerování), Uloží do místa umístění jaru (soubor = jmenoSouboru)
     *  - z exist. xml souboru, uloží místo původního (soubor = absolutni adresa)
     * @param koren
     * @param soubor
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void rootToHTML(Element koren,
                                  String soubor) throws FileNotFoundException,
                                                        IOException,
                                                        IllegalAddException,
                                                        XSLTransformException {
        docToFile(transform(koren, HTML_ROZPIS), soubor);
    }


    public static void rootToTableHTML(Element tabulka,
                                       String soubor) throws FileNotFoundException,
                                                             IOException,
                                                             XSLTransformException {
        docToFile(transform(tabulka, HTML_TABULKA), soubor);
    }


    private static Document transform(Element koren,
                                      String styl) throws XSLTransformException {
        Document doc = new Document(koren);
        try {
            XSLTransformer transformer = new XSLTransformer(styl);
            doc = transformer.transform(doc);
        } catch (XSLTransformException e) {
            throw e;
        }
        return doc;
    }

    private static void docToFile(Document doc,
                                  String soubor) throws FileNotFoundException,
                                                        IOException,
                                                        IllegalAddException {
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(soubor);
            XMLOutputter xout = new XMLOutputter(Format.getPrettyFormat());
            xout.output(doc, fout);
            fout.flush();
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            throw e;
        } catch (IllegalAddException e) {
            throw e;
        }
    }

    public static String[][] rootToArray(Element koren, int pocetRadku) {
        String[][] data = new String[pocetRadku][4];
        int radek = 0;
        int pocetKol = 1; // jednodussi, nez bez prom. - (i-1)*getPocetKol + j
        List<Element> periody = koren.getChild("rozlosovani").getChildren();
        for (int i = 0; i < periody.size(); i++) {
            data[radek++] = getRadek("", (i + 1) + ".", "perioda", "");
            List<Element> kola = periody.get(i).getChildren();
            for (int j = 0; j < kola.size(); j++) {
                data[radek++] = getRadek("", pocetKol++ + ".", "kolo", "");
                List<Element> zapasy = kola.get(j).getChildren();
                for (int k = 0; k < zapasy.size(); k++) {
                    Element domaci = zapasy.get(k).getChild("domaci");
                    Element hoste = zapasy.get(k).getChild("hoste");
                    data[radek++] =
                            getRadek(domaci.getChildText("jmenoD"), getVysledek(domaci.getChildText("golyD")),
                                     getVysledek(hoste.getChildText("golyH")),
                                     hoste.getChildText("jmenoH"));
                }
            }
        }
        return data;
    }

    private static String getVysledek(String goly) {
        return goly == null ? "-" : goly;
    }

    private static String[] getRadek(String u1, String u2, String u3,
                                     String u4) {
        return new String[] { u1, u2, u3, u4 };
    }

    public static Object[][] rootToTableArray(Element koren, Object[] tymy) {
        Tabulka tab = new Tabulka(tymy);
        List<Element> p = koren.getChild("rozlosovani").getChildren("perioda");
        for (int i = 0; i < p.size(); i++) {
            List<Element> kola = p.get(i).getChildren("kolo");
            for (int j = 0; j < kola.size(); j++) {
                List<Element> zapasy = kola.get(j).getChildren("zapas");
                boolean zapsano = false;
                for (int k = 0; k < zapasy.size(); k++) {
                    if (tab.zpracujZapas(zapasy.get(k))) {
                        zapsano = true;
                    }
                }
                if (!zapsano) {
                    // nebylo zapsan ani jeden vysledek z kola, proto dal neprochazi
                    return tab.getVysledkovaTabulka();
                }
            }
        }
        return tab.getVysledkovaTabulka();
    }

    /**
     * Vnitřní třída pro hlášky, které se vypíší po provedení metod
     */
    public static class Hlasky {

        public static String exportHTML(String soubor) {
            String t =
                "Vytvořili jste soubor '" + getJmenoSouboru(soubor) + "'\n";
            t +=
 "- html soubor můžete otevřít ve webovém prohlížeči a poté si ho vytisknout \n";
            t +=
 "- s html souborem už nemůžete dále rozpis upravovat (vkládat výsledky...) \n";
            t += "- soubor naleznete ve stejné složce jako tento program \n";
            return t;
        }

        public static String exportXML(String soubor) {
            String t =
                "Vytvořili jste soubor '" + getJmenoSouboru(soubor) + "'\n";
            t +=
 "- tento xml soubor můžete i později využít pro vkládání výsledků a tvorbu tabulky \n";
            t +=
 "- k práci s rozpisem se dostanete přes - Menu -> Upravit rozpis \n";
            t += "- soubor naleznete ve stejné složce jako tento program \n";
            return t;
        }

        public static String ulozeniHTML(String soubor) {
            String t =
                "Aktualizovali jste soubor '" + getJmenoSouboru(soubor) +
                "'\n";
            t +=
 "- html soubor můžete otevřít ve webovém prohlížeči a poté si ho vytisknout \n";
            t += "- umístění souboru: " + soubor;
            return t;
        }

        public static String ulozeniXML(String soubor) {
            String t =
                "Aktualizovali jste soubor '" + getJmenoSouboru(soubor) +
                "'\n";
            t += "- umístění souboru: " + soubor;
            return t;
        }

        public static String ulozeniTabulkaHtml(String soubor) {
            String t = "Vytvořili jste soubor '" + getJmenoSouboru(soubor) + "'\n";
            t +=
 "- html soubor si můžete otevřít ve webovém prohlížeči a tabulku si vytisknout \n";
            t +=
 "- doporučení: zvolte možnost tisku na šířku (tabulka bude přehlednější) \n";
            t += "- umístění souboru: " + soubor;
            return t;
        }

        private static String getJmenoSouboru(String soubor) {
            int pozice = soubor.lastIndexOf("/");
            return soubor.substring(pozice + 1);
        }
    }
}


