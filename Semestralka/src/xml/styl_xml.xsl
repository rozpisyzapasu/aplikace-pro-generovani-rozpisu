<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output indent="yes" method="xml" version="1.0" encoding="utf-8" />
  <!-- Root template -->
  <xsl:template match="/">     

   <xsl:element name="rozpis">
    <!-- kdyby se melo pridat informace o schema, tak pridat jako atribut rootu
    <xsl:attribute 
      name="xsi:noNamespaceSchemaLocation"
      namespace="http://www.w3.org/2001/XMLSchema-instance">
      <xsl:text>schema.xsd</xsl:text>
    </xsl:attribute>
    -->    
    <xsl:element name="informace">
     <xsl:element name="pocetPeriod"><xsl:value-of select="rozpis/informace/pocetPeriod" /></xsl:element>
     <xsl:element name="pocetKolPeriody"><xsl:value-of select="rozpis/informace/pocetKolPeriody" /></xsl:element>
     <xsl:element name="pocetZapasuKola"><xsl:value-of select="rozpis/informace/pocetZapasuKola" /></xsl:element>
     <xsl:element name="tymy">
      <xsl:attribute name="pocetTymu"><xsl:value-of select="rozpis/informace/tymy/@pocetTymu" /></xsl:attribute>
      <xsl:for-each select="rozpis/informace/tymy/tym">         
        <xsl:element name="tym"><xsl:attribute name="jmeno"><xsl:value-of select="@jmeno" /></xsl:attribute></xsl:element>
      </xsl:for-each>                
     </xsl:element>
    </xsl:element>

    <xsl:element name="rozlosovani">
     <xsl:for-each select="rozpis/rozlosovani/perioda">          
      <xsl:element name="perioda">
       <xsl:attribute name="cisloPeriody"><xsl:value-of select="@cisloPeriody" /></xsl:attribute>       
       
       <xsl:for-each select="kolo">          
       <xsl:element name="kolo">
       <xsl:attribute name="cisloKola"><xsl:value-of select="@cisloKola" /></xsl:attribute>
        
          <xsl:for-each select="zapas"> 
          <xsl:element name="zapas">
           <xsl:element name="domaci">            
            <xsl:element name="jmenoD"><xsl:value-of select="domaci/jmenoD" /></xsl:element>
            <xsl:element name="golyD"><xsl:value-of select="domaci/golyD" /></xsl:element>
           </xsl:element>
           <xsl:element name="hoste">
            <xsl:element name="jmenoH"><xsl:value-of select="hoste/jmenoH" /></xsl:element>
            <xsl:element name="golyH"><xsl:value-of select="hoste/golyH" /></xsl:element>
           </xsl:element>
          </xsl:element>
          </xsl:for-each>   
          
      </xsl:element> 
      </xsl:for-each>        
    </xsl:element>
    </xsl:for-each> 
    
    </xsl:element>
    
    </xsl:element>

  </xsl:template>
</xsl:stylesheet>
