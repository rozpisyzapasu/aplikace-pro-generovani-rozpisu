package generovani;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

public class Zapas {

    private int domaci;
    private int hoste;

    public Zapas(int domaci, int hoste) {
        this.domaci = domaci;
        this.hoste = hoste;
    }

    public void prohodDomaciHosty() {
        int pomocna = domaci;
        domaci = hoste;
        hoste = pomocna;
    }

    public int getDomaci() {
        return domaci;
    }

    public int getHoste() {
        return hoste;
    }

    @Override
    public String toString() {
        return domaci + " : " + hoste;
    }
}
