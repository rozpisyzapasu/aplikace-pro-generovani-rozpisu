package generovani;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Zdenda
 */
public class Kolo {
    private List<Zapas> zapasy;
    private int pocetZapasuKola;

    public Kolo(int pocetZapasuKola) {
        this.pocetZapasuKola = pocetZapasuKola;
        this.zapasy = new ArrayList<Zapas>(pocetZapasuKola);
    }

    public void vygenerujKolo(int[] tymy) {
        int pocetTymu = tymy.length;
        for (int i = 0; i < pocetZapasuKola; i++) {
            boolean sude = (i % 2 == 0);
            if (sude) {
                zapasy.add(i, new Zapas(tymy[i], tymy[pocetTymu - i - 1]));
            } else {
                zapasy.add(i, new Zapas(tymy[pocetTymu - i - 1], tymy[i]));
            }
        }
    }

    public void prohodDomaciHostyPrvnihoTymu() {
        zapasy.get(0).prohodDomaciHosty();
    }

    public Iterator<Zapas> getIteratorKola() {
        return zapasy.iterator();
    }

    @Override
    public String toString() {
        return zapasy.toString();
    }
}
