package generovani;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.text.Collator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import java.util.List;
import java.util.Locale;

import org.jdom.Element;

/**
 *
 * @author Zdenda
 */
public class Sezona {

    private List<String> tymy;
    private int pocetPeriod;
    private int pocetTymu; // tymy.size je vzdy sude, protoze prida tym volno
    private int pocetZapasuKola;
    private List<Kolo> rozpis;

    /**
     * Vytvoreni sezony, ktere pote i vygeneruje rozpis zapasu.
     * @param tymy
     * @param pocetPeriod
     * @param pocetZapasuKola
     */
    public Sezona(String[] tymy, int pocetPeriod, int pocetZapasuKola) {
        if (tymy.length < 2) {
            throw new IllegalArgumentException("Pocet tymu musi byt vetsi nebo roven 2!!!");
        }
        // inicializace
        this.pocetZapasuKola = pocetZapasuKola;
        this.pocetTymu = tymy.length;
        this.pocetPeriod = pocetPeriod;
        // nacteni tymu a promíchání
        nactiTymy(tymy);
        // generovani
        int pocetKol = (pocetTymu % 2 == 0) ? pocetTymu - 1 : pocetTymu;
        this.rozpis = new ArrayList<Kolo>(pocetKol);
        vygenerujRozpis(pocetKol);
    }

    /**
     * vygeneruje vsechna kola pro jednu periodu
     * @param pocetKol
     */
    private void vygenerujRozpis(int pocetKol) {
        int[] tymyPomocnePole = vytvorPomocnePole();
        int pocetZapasu = tymy.size() / 2; // (tymy.size(), protoze i + VOLNO
        for (int i = 0; i < pocetKol; i++) {
            rozpis.add(i, new Kolo(pocetZapasu));
            rozpis.get(i).vygenerujKolo(tymyPomocnePole);
            tymyPomocnePole = pootocPoleTymu(tymyPomocnePole);
        }
        vyrovnejBilanciPrvnihoTymu(pocetKol);
    }

    /**
     * Priklad:[1 2 3 4 5 6] -> zapasy 1-6, 2-5, 3-4
     * @return [1 6 2 3 4 5] -> zapasy 1-5, 6-4, 2-3
     */
    private int[] pootocPoleTymu(int[] tymyPomocnePole) {
        int pomocny = tymyPomocnePole[tymy.size() - 1];
        for (int i = tymy.size() - 1; i > 1; i--) {
            tymyPomocnePole[i] = tymyPomocnePole[i - 1];
        }
        tymyPomocnePole[1] = pomocny;
        return tymyPomocnePole;
    }

    /**
     * Aby neměl první tým všechny zápasy doma (nebo venk), tak u každého druhého
     * kola prohodím domácí a hosty prvního zápasu
     */
    private void vyrovnejBilanciPrvnihoTymu(int pocetKol) {
        for (int i = 0; i < pocetKol; i++) {
            if (i % 2 == 0) {
                rozpis.get(i).prohodDomaciHostyPrvnihoTymu();
            }
        }
    }

    /**
     * načtení týmů, pro lichý počet přidá tým VOLNO
     */
    private void nactiTymy(String[] tymy) {
        this.tymy = new ArrayList<String>();
        for (int i = 0; i < tymy.length; i++) {
            this.tymy.add(i, tymy[i]);
        }
        // pro pripad, ze je lichypocet tymu
        if (tymy.length % 2 == 1) {
            this.tymy.add(tymy.length, "VOLNO");
        }
        Collections.shuffle(this.tymy);
    }

    private int[] vytvorPomocnePole() {
        int[] tymyPomocnePole = new int[tymy.size()];
        for (int i = 0; i < this.tymy.size(); i++) {
            tymyPomocnePole[i] = i;
        }
        return tymyPomocnePole;
    }

    /**
     * Algoritmus vygeneruje pro počet zapasu v kole = (pocetTymu / 2), ale
     * uzivatel muze pocet zapasu v kole omezit, a tak se muze stat, ze musi
     * pridat kolo(a) navic
     * @return skutecny pocet kol jedne periody
     */
    private int getPocetKolPeriody() {
        int pocet = getPocetZapasuPeriody() / pocetZapasuKola;
        if (pocet * pocetZapasuKola != getPocetZapasuPeriody()) {
            pocet++;
        }
        return pocet;
    }

    /**
     * nemuzu pocitat s model.size(), protoze to je vzdy sude cislo (pridal jsem VOLNO)
     */
    private int getPocetZapasuPeriody() {
        return pocetTymu * (pocetTymu - 1) / 2;
    }

    /**
     * @return koren obsahujici informace o rozpisu i cele rozlosovani
     */
    public Element getKoren() {
        Element koren = new Element("rozpis");
        Element informace = getInformace();
        Element rozlosovani = getRozlosovani();
        koren.addContent(informace);
        koren.addContent(rozlosovani);
        return koren;
    }

    private Element getInformace() {
        Element informace = new Element("informace");
        Element periody =
            new Element("pocetPeriod").addContent("" + pocetPeriod);
        Element kola =
            new Element("pocetKolPeriody").addContent("" + getPocetKolPeriody());
        Element zapasyKola =
            new Element("pocetZapasuKola").addContent("" + pocetZapasuKola);
        Element tymy = getTymy();
        informace.addContent(periody);
        informace.addContent(kola);
        informace.addContent(zapasyKola);
        informace.addContent(tymy);
        return informace;
    }

    private Element getTymy() {
        // seřadim tymy
        Collections.sort(tymy, Collator.getInstance(new Locale("cs", "CZ")));
        // zapis
        Element eTymy = new Element("tymy");
        eTymy.setAttribute("pocetTymu", "" + pocetTymu);
        Iterator<String> it = tymy.iterator();
        Element tym;
        while (it.hasNext()) {
            String t = it.next();
            if (!t.equals("VOLNO")) {
                tym = new Element("tym");
                tym.setAttribute("jmeno", t);
                eTymy.addContent(tym);
            }
        }
        return eTymy;
    }

    /**
     * abych nemusel zkoumat tým VOLNO, tak využiji pole generované pro TableModel
     * @return
     */
    private Element getRozlosovani() {
        int pocetKol = 1;
        int iVolno = getIndexVolnoTymu();
        boolean lichyPocetTymu = pocetTymu % 2 == 1;
        Element rozlosovani = new Element("rozlosovani");
        for (int i = 1; i <= pocetPeriod; i++) {
            Element perioda = getPerioda(i);
            Iterator<Kolo> itKol = this.rozpis.iterator();
            Iterator<Zapas> itZapasy = itKol.next().getIteratorKola();
            for (int j = 1; j <= getPocetKolPeriody(); j++) {
                Element kolo = getKolo(pocetKol++);
                for (int k = 1; k <= pocetZapasuKola; k++) {
                    boolean zapsano = false;
                    do {
                        if (!itZapasy.hasNext()) {
                            if (itKol.hasNext()) {
                                itZapasy = itKol.next().getIteratorKola();
                            } else {
                                // pro pripad, ze kolo neobsahuje max pocet zapasu
                                break;
                            }
                        }
                        Zapas z = itZapasy.next();
                        if (lichyPocetTymu &&
                            !jdeZapsatZapas(z.getDomaci(), z.getHoste(),
                                            iVolno)) {
                            // kdyz nejde zapsat, tak jedu znova, protoze jsem nezapsal k-ty zapas v j-tem kole
                            continue;
                        }
                        String domaci, hoste;
                        if (i % 2 == 0) {
                            domaci = tymy.get(z.getHoste());
                            hoste = tymy.get(z.getDomaci());
                        } else {
                            domaci = tymy.get(z.getDomaci());
                            hoste = tymy.get(z.getHoste());
                        }
                        kolo.addContent(getZapas(k, domaci, hoste));
                        zapsano = true;
                    } while (!zapsano);
                }
                perioda.addContent(kolo);
            }
            rozlosovani.addContent(perioda);
        }
        return rozlosovani;
    }

    /**
     * ke zjisteni tymu volna, abych nemusel u rozpisu s lichym poctem tymu, zkoumat,
     * zda tym.equals("VOLNO"), ale muzu to jednoduse zkontrolovat jen pres index
     */
    private int getIndexVolnoTymu() {
        if (pocetTymu % 2 == 1) {
            return tymy.indexOf("VOLNO");
        }
        return -1;
    }

    private boolean jdeZapsatZapas(int domaci, int hoste, int indexTymVolno) {
        if (domaci != indexTymVolno && hoste != indexTymVolno) {
            return true;
        } else {
            return false;
        }
    }

    private Element getPerioda(Integer cislo) {
        return new Element("perioda").setAttribute("cisloPeriody",
                                                   cislo.toString());
    }

    private Element getKolo(Integer cislo) {
        return new Element("kolo").setAttribute("cisloKola", cislo.toString());
    }

    private Element getZapas(Integer cislo, String dom, String hos) {
        Element zapas = new Element("zapas");
        zapas.setAttribute("cisloZapasu", cislo.toString());
        // domaci
        Element domaci = new Element("domaci");
        Element jmenoD = new Element("jmenoD");
        jmenoD.addContent(dom);
        Element golyD = new Element("golyD");
        golyD.addContent("");
        domaci.addContent(jmenoD);
        domaci.addContent(golyD);
        // hoste
        Element hoste = new Element("hoste");
        Element jmenoH = new Element("jmenoH");
        jmenoH.addContent(hos);
        Element golyH = new Element("golyH");
        golyH.addContent("");
        hoste.addContent(jmenoH);
        hoste.addContent(golyH);
        // pripojeni k zapasu
        zapas.addContent(domaci);
        zapas.addContent(hoste);
        return zapas;
    }
}
