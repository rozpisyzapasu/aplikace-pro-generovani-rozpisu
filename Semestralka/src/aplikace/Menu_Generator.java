package aplikace;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import generovani.Sezona;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.io.IOException;

import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdom.Element;
import org.jdom.JDOMException;

import tabulka.NahledJPanel;

import xml.Export;
import xml.RozpisXML;

public class Menu_Generator extends JPanel {

    private JLabel jLabelPopis_upravaAktualni = new JLabel();
    private JLabel jLabelPopis_VlozeneTymy = new JLabel();
    private JLabel jLabelPopis_autoNazvy = new JLabel();
    private JLabel jLabelPopis_InfoOTymech = new JLabel();
    private JLabel jLabelPopis_NovyTymy = new JLabel();
    private JLabel jLabelPopis_ZakladniNastaveni = new JLabel();
    private JCheckBox automatickaJmenaCheckBox = new JCheckBox();
    private JLabel pocetZbyvajicichJLabel = new JLabel();
    private JLabel pocetVlozenychLabel = new JLabel();
    private JLabel jLabelPopis_PocetZbyvajicich = new JLabel();
    private JLabel jLabelPopis_PocetVlozenych = new JLabel();
    private JButton vlozJButton = new JButton();
    private JTextField nazevTymuJField = new JTextField();
    private JLabel jLabelPopis_PocetTymu = new JLabel();
    private JLabel jLabelPopis_PocetPeriod = new JLabel();
    private JLabel jLabelPopis_PocetZapasuKola = new JLabel();
    private JComboBox pocetZapasuJComboBox = new JComboBox();
    private JComboBox periodyJComboBox = new JComboBox();
    private JComboBox tymyJComboBox = new JComboBox();
    private JButton vymazatVybranyJButton = new JButton();
    private JButton zmenaJButton = new JButton();
    private JTextField upravovanyTymJField = new JTextField();
    private JButton vygenerovatJButton = new JButton();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private JList seznamTymuJList = new JList();
    private JPanel generovaniJPanel = new JPanel();
    private JButton htmlJButton = new JButton();
    private JButton xmlJButton = new JButton();
    private JButton nahledJButton = new JButton();
    private JSeparator jSeparator3 = new JSeparator();
    private JSeparator jSeparator5 = new JSeparator();
    private JSeparator jSeparator4 = new JSeparator();
    private JSeparator jSeparator1 = new JSeparator();
    private JSeparator jSeparator6 = new JSeparator();
    private JSeparator jSeparator2 = new JSeparator();
    // pomocne
    private boolean automatickeNazvy;
    private DefaultListModel model = new DefaultListModel();
    //private Sezona sezona;
    private RozpisXML xmlSoubor;

    public Menu_Generator() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(null);
        this.setSize(new Dimension(511, 399));
        jLabelPopis_upravaAktualni.setText("Aktuálně vybraný tým ze seznamu:");
        jLabelPopis_upravaAktualni.setForeground(new Color(0, 0, 82));
        jLabelPopis_upravaAktualni.setBounds(new Rectangle(270, 210, 170, 15));
        jLabelPopis_VlozeneTymy.setText("Vložené týmy:");
        jLabelPopis_VlozeneTymy.setForeground(new Color(0, 0, 82));
        jLabelPopis_VlozeneTymy.setBounds(new Rectangle(275, 10, 70, 15));
        jLabelPopis_autoNazvy.setText("Generování automatických názvů týmů:");
        jLabelPopis_autoNazvy.setForeground(new Color(0, 0, 82));
        jLabelPopis_autoNazvy.setBounds(new Rectangle(10, 240, 190, 15));
        jLabelPopis_InfoOTymech.setText("Informace o seznamu týmů:");
        jLabelPopis_InfoOTymech.setForeground(new Color(0, 0, 82));
        jLabelPopis_InfoOTymech.setBounds(new Rectangle(10, 170, 135, 15));
        jLabelPopis_NovyTymy.setText("Vložení nového týmu:");
        jLabelPopis_NovyTymy.setForeground(new Color(0, 0, 82));
        jLabelPopis_NovyTymy.setBounds(new Rectangle(10, 110, 105, 15));
        jLabelPopis_ZakladniNastaveni.setText("Základní nastavení:");
        jLabelPopis_ZakladniNastaveni.setForeground(new Color(0, 0, 82));
        jLabelPopis_ZakladniNastaveni.setBounds(new Rectangle(10, 10, 95, 15));
        automatickaJmenaCheckBox.setText("Chci automatické názvy (Tým1, Tým2, ...)");
        automatickaJmenaCheckBox.setActionCommand("A");
        automatickaJmenaCheckBox.setBounds(new Rectangle(10, 260, 235, 20));
        automatickaJmenaCheckBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    automatickeNazvyCheckBox1_actionPerformed(e);
                }
            });
        pocetZbyvajicichJLabel.setText("0");
        pocetZbyvajicichJLabel.setBounds(new Rectangle(210, 210, 40, 15));
        pocetVlozenychLabel.setText("0");
        pocetVlozenychLabel.setBounds(new Rectangle(210, 190, 35, 15));
        jLabelPopis_PocetZbyvajicich.setText("Ještě vám zbývá vložit:");
        jLabelPopis_PocetZbyvajicich.setBounds(new Rectangle(10, 210, 135,
                                                             15));
        jLabelPopis_PocetVlozenych.setText("Počet vložených týmů: ");
        jLabelPopis_PocetVlozenych.setBounds(new Rectangle(10, 190, 135, 15));
        vlozJButton.setText("Vložit tým");
        vlozJButton.setBounds(new Rectangle(165, 130, 80, 20));
        vlozJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    vlozJButton_actionPerformed(e);
                }
            });
        nazevTymuJField.setBounds(new Rectangle(10, 130, 145, 20));
        nazevTymuJField.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    nazevTymuJField_actionPerformed(e);
                }
            });
        jLabelPopis_PocetTymu.setText("Počet týmů:");
        jLabelPopis_PocetTymu.setBounds(new Rectangle(10, 30, 60, 15));
        jLabelPopis_PocetPeriod.setText("Počet period:");
        jLabelPopis_PocetPeriod.setBounds(new Rectangle(10, 55, 65, 15));
        jLabelPopis_PocetZapasuKola.setText("Počet zápasů v kole:");
        jLabelPopis_PocetZapasuKola.setBounds(new Rectangle(10, 80, 100, 15));
        pocetZapasuJComboBox.setBounds(new Rectangle(165, 75, 80, 20));
        pocetZapasuJComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    pocetZapasuJComboBox_actionPerformed(e);
                }
            });
        periodyJComboBox.setBounds(new Rectangle(165, 50, 80, 20));
        periodyJComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    periodyJComboBox_actionPerformed(e);
                }
            });
        tymyJComboBox.setSize(new Dimension(65, 20));
        tymyJComboBox.setBounds(new Rectangle(165, 25, 80, 20));
        tymyJComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    tymyJComboBox_actionPerformed(e);
                }
            });
        vymazatVybranyJButton.setText("Vymazat");
        vymazatVybranyJButton.setBounds(new Rectangle(390, 255, 110, 20));
        vymazatVybranyJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    vymazatVybranyJButton_actionPerformed(e);
                }
            });
        zmenaJButton.setText("Uložit změnu");
        zmenaJButton.setBounds(new Rectangle(275, 255, 110, 20));
        zmenaJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    zmenaJButton_actionPerformed(e);
                }
            });
        upravovanyTymJField.setHorizontalAlignment(JTextField.LEFT);
        upravovanyTymJField.setBounds(new Rectangle(275, 230, 225, 20));
        vygenerovatJButton.setText("Vygenerovat rozpis");
        vygenerovatJButton.setEnabled(false);
        vygenerovatJButton.setBounds(new Rectangle(275, 295, 225, 20));
        vygenerovatJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    vygenerovatJButton_actionPerformed(e);
                }
            });
        jScrollPane1.setBounds(new Rectangle(275, 25, 225, 180));
        seznamTymuJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        seznamTymuJList.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    seznamTymuJList_mouseClicked(e);
                }
            });
        generovaniJPanel.setBounds(new Rectangle(20, 345, 480, 35));
        generovaniJPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0,
                                                                   0));
        htmlJButton.setText("Uložit jako HTML soubor");
        htmlJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    htmlJButton_actionPerformed(e);
                }
            });
        xmlJButton.setText("Uložit jako XML soubor");
        xmlJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    xmlJButton_actionPerformed(e);
                }
            });
        nahledJButton.setText("Náhled rozpisu");
        nahledJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    nahledJButton_actionPerformed(e);
                }
            });
        jSeparator3.setForeground(new Color(214, 214, 214));
        jSeparator3.setBounds(new Rectangle(5, 285, 520, 25));
        jSeparator5.setForeground(new Color(214, 214, 214));
        jSeparator5.setBounds(new Rectangle(0, 230, 255, 10));
        jSeparator4.setForeground(new Color(214, 214, 214));
        jSeparator4.setBounds(new Rectangle(0, 160, 255, 5));
        jSeparator1.setForeground(new Color(214, 214, 214));
        jSeparator1.setBounds(new Rectangle(5, 100, 250, 25));
        jSeparator6.setOrientation(SwingConstants.VERTICAL);
        jSeparator6.setForeground(new Color(214, 214, 214));
        jSeparator6.setBounds(new Rectangle(255, 5, 2, 280));
        jSeparator2.setForeground(new Color(214, 214, 214));
        jSeparator2.setBounds(new Rectangle(0, 5, 525, 10));
        jScrollPane1.getViewport().add(seznamTymuJList, null);
        generovaniJPanel.add(nahledJButton, null);
        generovaniJPanel.add(xmlJButton, null);
        generovaniJPanel.add(htmlJButton, null);
        this.add(jSeparator2, null);
        this.add(jSeparator6, null);
        this.add(jSeparator1, null);
        this.add(jSeparator4, null);
        this.add(jSeparator5, null);
        this.add(jSeparator3, null);
        this.add(generovaniJPanel, null);
        this.add(jScrollPane1, null);
        this.add(vygenerovatJButton, null);
        this.add(upravovanyTymJField, null);
        this.add(zmenaJButton, null);
        this.add(vymazatVybranyJButton, null);
        this.add(tymyJComboBox, null);
        this.add(periodyJComboBox, null);
        this.add(pocetZapasuJComboBox, null);
        this.add(jLabelPopis_PocetZapasuKola, null);
        this.add(jLabelPopis_PocetPeriod, null);
        this.add(jLabelPopis_PocetTymu, null);
        this.add(nazevTymuJField, null);
        this.add(vlozJButton, null);
        this.add(jLabelPopis_PocetVlozenych, null);
        this.add(jLabelPopis_PocetZbyvajicich, null);
        this.add(pocetVlozenychLabel, null);
        this.add(pocetZbyvajicichJLabel, null);
        this.add(automatickaJmenaCheckBox, null);
        this.add(jLabelPopis_ZakladniNastaveni, null);
        this.add(jLabelPopis_NovyTymy, null);
        this.add(jLabelPopis_InfoOTymech, null);
        this.add(jLabelPopis_autoNazvy, null);
        this.add(jLabelPopis_VlozeneTymy, null);
        this.add(jLabelPopis_upravaAktualni, null);
        myInit();
    }

    private void myInit() {
        for (int i = 4; i <= 60; i++) {
            tymyJComboBox.addItem(i);
        }
        for (int i = 1; i <= 10; i++) {
            periodyJComboBox.addItem(i);
        }
        nastavPocetZapasu();
        skryjGenerovaciPanel();
    }

    private int getAktualniPocetTymu() {
        return tymyJComboBox.getSelectedIndex() + 4;
    }

    private void nastavPocetZapasu() {
        int pocetTymu = getAktualniPocetTymu();
        int max = pocetTymu / 2;
        pocetZapasuJComboBox.removeAllItems();
        for (int i = 2; i <= max; i++) {
            pocetZapasuJComboBox.addItem(i);
        }
        pocetZapasuJComboBox.setSelectedItem(max);
    }

    // AUTOMATICKE NAZVY

    private void automatickeNazvyCheckBox1_actionPerformed(ActionEvent e) {
        if (automatickaJmenaCheckBox.isSelected()) {
            automatickeNazvy = true;
            nastavGUI(false);
        } else {
            automatickeNazvy = false;
            nastavGUI(true);
        }
        overPlnySeznam();
        skryjGenerovaciPanel();
    }

    private void nastavGUI(boolean stav) {
        vlozJButton.setEnabled(stav);
        zmenaJButton.setEnabled(stav);
        vymazatVybranyJButton.setEnabled(stav);
        seznamTymuJList.setEnabled(stav);
        nazevTymuJField.setEnabled(stav);
        upravovanyTymJField.setEnabled(stav);
        if (stav) {
            pocetVlozenychLabel.setText("" + model.size());
            int pocetTymu = getAktualniPocetTymu();
            pocetZbyvajicichJLabel.setText("" + (pocetTymu - model.size()));
            pocetVlozenychLabel.setForeground(Color.BLACK);
            pocetZbyvajicichJLabel.setForeground(Color.BLACK);
        } else {
            pocetVlozenychLabel.setText("" + 0);
            pocetZbyvajicichJLabel.setText("" + 0);
            pocetVlozenychLabel.setForeground(Color.GRAY);
            pocetZbyvajicichJLabel.setForeground(Color.GRAY);
        }
    }

    // VKLADANI

    private void vlozJButton_actionPerformed(ActionEvent e) {
        vlozTym();
    }

    private void nazevTymuJField_actionPerformed(ActionEvent e) {
        vlozTym();
    }

    /**
     * metoda zajišťuje vložení týmu do JLIstu
     */
    private void vlozTym() {
        int aktualniPocet = getAktualniPocetTymu();
        String novyTym = nazevTymuJField.getText();
        if (novyTym.isEmpty()) { // vlozeni prazdneho tymu
            showInfoPanel("Upozornění", "Proč vkládáte prázdný název?");
        } else if (model.size() >= aktualniPocet) { // vlozeni do plneho
            showInfoPanel("Upozornění",
                          "Váš seznam už je plný - buď vygenerujte rozpis anebo zvyšte počet týmů");
        } else if (jeTymVListu(novyTym)) { // vlozeni duplicitního týmu
            showInfoPanel("Upozornění",
                          "Tým '" + novyTym + "' už jste vložili.");
            vymazJTextField(nazevTymuJField);
        } else { // vložení kde je všechno OK
            model.addElement(novyTym);
            seznamTymuJList.setModel(model);
            vymazJTextField(nazevTymuJField);
            aktualizujInformace();
            overPlnySeznam();
        }
        skryjGenerovaciPanel();
    }

    private boolean jeTymVListu(Object tym) {
        return model.contains(tym);
    }

    private void overPlnySeznam() {
        int aktualniPocet = getAktualniPocetTymu();
        if (model.size() == aktualniPocet || automatickeNazvy == true) {
            vygenerovatJButton.setEnabled(true);
        } else {
            vygenerovatJButton.setEnabled(false);
        }
    }

    // COMBOBOXY

    private void tymyJComboBox_actionPerformed(ActionEvent e) {
        int vybrano = getAktualniPocetTymu();
        pocetZbyvajicichJLabel.setText("" + (vybrano - model.size()));
        nastavPocetZapasu();
        if (automatickeNazvy) {
            vygenerovatJButton.setEnabled(true);
            pocetZbyvajicichJLabel.setText("" + 0);
        } else if (model.size() == vybrano) {
            vygenerovatJButton.setEnabled(true);
        } else if (model.size() > vybrano) {
            tymyJComboBox.setSelectedItem(model.size());
            pocetZbyvajicichJLabel.setText("0");
            vygenerovatJButton.setEnabled(true);
            showInfoPanel("Upozornění",
                          "Počet týmů v seznamu je větší než vámi zvolený počet");
        } else {
            vygenerovatJButton.setEnabled(false);
        }
        if (!automatickaJmenaCheckBox.isEnabled()) {
            aktualizujInformace();
        }
        skryjGenerovaciPanel();
    }

    private void periodyJComboBox_actionPerformed(ActionEvent e) {
        skryjGenerovaciPanel();
    }

    private void pocetZapasuJComboBox_actionPerformed(ActionEvent e) {
        skryjGenerovaciPanel();
    }

    // JLIST + UPRAVY JLISTU

    private void seznamTymuJList_mouseClicked(MouseEvent e) {
        if (seznamTymuJList.getSelectedValue() != null) {
            upravovanyTymJField.setText("" +
                                        seznamTymuJList.getSelectedValue());
        }
        skryjGenerovaciPanel();
    }

    private void vymazatVybranyJButton_actionPerformed(ActionEvent e) {
        if (seznamTymuJList.isSelectionEmpty()) {
            showInfoPanel("Upozornění", "Nemáte vybraný žádný prvek");
        } else {
            Object aktualneVybrany = seznamTymuJList.getSelectedValue();
            model.removeElement(aktualneVybrany);
            vymazJTextField(upravovanyTymJField);
            aktualizujInformace();
            vygenerovatJButton.setEnabled(false);
        }
        skryjGenerovaciPanel();
    }

    private void zmenaJButton_actionPerformed(ActionEvent e) {
        Object aktualneVybrany = seznamTymuJList.getSelectedValue();
        Object zmeneny = upravovanyTymJField.getText();
        if (zmeneny.equals("")) {
            showInfoPanel("Upozornění",
                          "Proč vkládáte prázdný název? Jestli chcete prvek zrušit, tak klikněte na Vymazat");
            upravovanyTymJField.setText("" + aktualneVybrany);
        } else if (seznamTymuJList.isSelectionEmpty()) {
            showInfoPanel("Upozornění", "Nemáte vybraný žádný prvek");
        } else if (aktualneVybrany.equals(zmeneny)) {
            showInfoPanel("Upozornění", "Neprovedli jste žádnou změnu");
        } else if (jeTymVListu(zmeneny)) {
            showInfoPanel("Upozornění",
                          "Tým '" + zmeneny + "' už se v seznamu nachází.");
        } else {
            int aktualniIndex = seznamTymuJList.getSelectedIndex();
            model.add(aktualniIndex, zmeneny);
            model.removeElement(aktualneVybrany);
            upravovanyTymJField.setText("");
            aktualizujInformace();
        }
        skryjGenerovaciPanel();
    }

    // GENEROVANI

    private void vygenerovatJButton_actionPerformed(ActionEvent e) {
        try {
            xmlSoubor =
                    new RozpisXML(vygenerujSezonu(), new URI(getNazevSouboru()).getPath());
        } catch (JDOMException f) {
            showErrorPanel("JDOMException", f.getMessage());
        } catch (IOException f) {
            showErrorPanel("IOException", f.getMessage());
        } catch (URISyntaxException f) {
            showErrorPanel("URISyntaxException", f.getMessage());
        }
        generovaniJPanel.setVisible(true);
    }

    /**
     * vygenerovani samotneho rozpisu sezony
     */
    private Element vygenerujSezonu() {
        int pocetPeriod = periodyJComboBox.getSelectedIndex() + 1;
        int pocetZapasuKola = pocetZapasuJComboBox.getSelectedIndex() + 2;
        String[] tymy = vytvorPoleTymu(model.toArray());
        Sezona sezona = new Sezona(tymy, pocetPeriod, pocetZapasuKola);
        return sezona.getKoren();
    }

    private String[] vytvorPoleTymu(Object[] objects) {
        int pocetTymu = getAktualniPocetTymu();
        String[] tymy = new String[pocetTymu];
        for (int i = 0; i < pocetTymu; i++) {
            tymy[i] =
                    (automatickeNazvy) ? ("Tým " + (i + 1)) : objects[i].toString();
        }
        return tymy;
    }

    private void htmlJButton_actionPerformed(ActionEvent e) {
        Exception f = xmlSoubor.exportHTML();
        if (f == null) {
            showInfoPanel("Export do HTML",
                          Export.Hlasky.exportHTML(xmlSoubor.getHtmlAdresa()));
        } else {
            showErrorPanel("Chyba při exportu do HTML", f.getMessage());
        }
    }

    private void xmlJButton_actionPerformed(ActionEvent e) {
        Exception f = xmlSoubor.exportXML();
        if (f == null) {
            showInfoPanel("Export do XML",
                          Export.Hlasky.exportXML(xmlSoubor.getXmlAdresa()));
        } else {
            showErrorPanel("Chyba při exportu do XML", f.getMessage());
        }
    }

    private void nahledJButton_actionPerformed(ActionEvent e) {
        NahledJPanel nahled = new NahledJPanel();
        nahled.setData(xmlSoubor.getRozpis());
        showInfoPanel("Náhled rozpisu", nahled);
    }

    private String getNazevSouboru() {
        String nazev = "";
        do {
            nazev =
                    JOptionPane.showInputDialog(this, "Zadejte, jak se má vás soubor jmenovat",
                                                "Zadání jména souboru", 1);
            if (nazev == null) {
                nazev = "";
            }
        } while (nazev.isEmpty());
        nazev = nazev.replaceAll(" ", "-");
        return nazev + ".xml";
    }

    // pomocne metody

    /**
     * metoda pro zobrazení okna s informační zprávou
     */
    private void showInfoPanel(String title, Object zprava) {
        JOptionPane.showMessageDialog(this, zprava, title,
                                      JOptionPane.PLAIN_MESSAGE);
    }

    private void showErrorPanel(String title, Object zprava) {
        JOptionPane.showMessageDialog(this, zprava, title,
                                      JOptionPane.ERROR_MESSAGE);
    }

    private void vymazJTextField(JTextField tf) {
        tf.setText("");
    }

    private void aktualizujInformace() {
        int aktualniPocet = getAktualniPocetTymu();
        pocetVlozenychLabel.setText("" + model.size());
        pocetZbyvajicichJLabel.setText("" + (aktualniPocet - model.size()));
    }

    private void skryjGenerovaciPanel() {
        generovaniJPanel.setVisible(false);
    }
}
