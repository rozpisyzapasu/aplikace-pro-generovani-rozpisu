package aplikace;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jdom.JDOMException;
import tabulka.*;
import xml.Export;
import xml.RozpisXML;
import xml.DOMVerificator;

public class Menu_Upravy extends JPanel {

    // horni menu - vyber souboru
    private JButton vybratJButton = new JButton();
    private JLabel jmenoSouboruJLabel = new JLabel();
    private JLabel jLabelPopis_InfoSoubor = new JLabel();
    // separatory - horni a dolni menu
    private JSeparator jSeparator5 = new JSeparator();
    private JSeparator jSeparator6 = new JSeparator();
    // JTABPANE
    private JTabbedPane jTabbedPane = new JTabbedPane();
    // JTAB - informace o rozpisu
    private JPanel informaceRozpisJPanel = new JPanel();
    private JButton zobrazitTymyJButton = new JButton();
    private JLabel jLabelPopis_InfoOTymech = new JLabel();
    private JLabel jLabelPopis_InfoORozpisu = new JLabel();
    private JLabel jLabelPopis_PocetKolPeriody = new JLabel();
    private JLabel jLabelPopis_PocetPeriod = new JLabel();
    private JLabel jLabelPopis_PocetZapasuKola = new JLabel();
    private JLabel jLabelPopis_PocetTymu = new JLabel();
    private JLabel jLabelPopis_CelkemZapasu = new JLabel();
    private JLabel pocetKolPeriodyLabel = new JLabel();
    private JLabel pocetPeriodLabel = new JLabel();
    private JLabel pocetTymuLabel = new JLabel();
    private JLabel pocetZapasuKolaLabel = new JLabel();
    private JLabel celkemZapasuJLabel = new JLabel();
    // JTAB - rozpis
    private JPanel koloJPanel = new JPanel();
    private JPanel volbaKolaJPanel = new JPanel();
    private JLabel jLabelPopis_InfoNastaveniKola = new JLabel();
    private JLabel jLabelPopis_KoloVyber = new JLabel();
    private JLabel jLabelPopis_PeriodaVyber = new JLabel();
    private JComboBox kolaJComboBox = new JComboBox();
    private JComboBox periodyJComboBox = new JComboBox();
    private JScrollPane koloInnerJScrollPane = new JScrollPane();
    // JTAB - tabulka
    private JScrollPane tabulkaJScrollPane = new JScrollPane();
    // dolni menu - ulozeni atd.
    private JButton ulozTabulkuJButton = new JButton();
    private JButton htmlJButton = new JButton();
    private JButton nahledJButton = new JButton();
    private JButton xmlJButton = new JButton();
    // xml a vlastni JTable
    private RozpisXML xmlSoubor;
    private RozpisJTable zapasyJTable = new RozpisJTable(true);
    private TabulkaJTable tabulkaJTable = new TabulkaJTable();

    public Menu_Upravy() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception, NoClassDefFoundError {
        this.setLayout(null);
        this.setSize(new Dimension(517, 403));
        vybratJButton.setText("Vybrat");
        vybratJButton.setBounds(new Rectangle(10, 5, 145, 20));
        vybratJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    vybratJButotn_actionPerformed(e);
                }
            });
        jmenoSouboruJLabel.setText("Nebyl načten žádný soubor");
        jmenoSouboruJLabel.setBounds(new Rectangle(265, 10, 240, 15));
        jLabelPopis_InfoORozpisu.setText("Informace o rozpisu:");
        jLabelPopis_InfoORozpisu.setForeground(new Color(0, 0, 82));
        jLabelPopis_InfoORozpisu.setBounds(new Rectangle(10, 5, 130, 20));
        htmlJButton.setText("Uložit rozpis HTML");
        htmlJButton.setBounds(new Rectangle(260, 375, 120, 20));
        htmlJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    htmlJButton_actionPerformed(e);
                }
            });
        nahledJButton.setText("Náhled rozpisu");
        nahledJButton.setBounds(new Rectangle(145, 375, 105, 20));
        nahledJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    nahledJButton_actionPerformed(e);
                }
            });
        xmlJButton.setText("Uložit rozpis XML");
        xmlJButton.setBounds(new Rectangle(390, 375, 115, 20));
        xmlJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    xmlJButton_actionPerformed(e);
                }
            });
        jLabelPopis_PocetKolPeriody.setText("Počet kol periody:");
        jLabelPopis_PocetKolPeriody.setBounds(new Rectangle(10, 55, 95, 15));
        pocetKolPeriodyLabel.setText("0");
        pocetKolPeriodyLabel.setBounds(new Rectangle(150, 55, 45, 15));
        pocetPeriodLabel.setText("0");
        pocetPeriodLabel.setBounds(new Rectangle(150, 35, 45, 15));
        jLabelPopis_PocetPeriod.setText("Počet period:");
        jLabelPopis_PocetPeriod.setBounds(new Rectangle(10, 30, 65, 20));
        pocetTymuLabel.setText("0");
        pocetTymuLabel.setBounds(new Rectangle(150, 175, 30, 15));
        pocetZapasuKolaLabel.setText("0");
        pocetZapasuKolaLabel.setBounds(new Rectangle(150, 80, 45, 15));
        jLabelPopis_PocetZapasuKola.setText("Počet zápasů kola:");
        jLabelPopis_PocetZapasuKola.setBounds(new Rectangle(10, 75, 90, 20));
        jLabelPopis_PocetTymu.setText("Počet týmů:");
        jLabelPopis_PocetTymu.setBounds(new Rectangle(10, 175, 85, 20));
        jLabelPopis_CelkemZapasu.setText("Celkem zápasů:");
        jLabelPopis_CelkemZapasu.setBounds(new Rectangle(10, 95, 75, 20));
        celkemZapasuJLabel.setText("0");
        celkemZapasuJLabel.setBounds(new Rectangle(150, 100, 45, 15));
        jLabelPopis_InfoSoubor.setText("Vybraný soubor:");
        jLabelPopis_InfoSoubor.setForeground(new Color(0, 0, 82));
        jLabelPopis_InfoSoubor.setBounds(new Rectangle(175, 10, 95, 15));
        jSeparator5.setForeground(new Color(214, 214, 214));
        jSeparator5.setBounds(new Rectangle(5, 30, 500, 10));
        jLabelPopis_KoloVyber.setText("Kolo:");
        jLabelPopis_PeriodaVyber.setText("Perioda:");
        kolaJComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    kolaJComboBox_actionPerformed(e);
                }
            });
        periodyJComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    periodyJComboBox_actionPerformed(e);
                }
            });
        koloJPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        koloJPanel.setBackground(Color.white);
        koloJPanel.setLayout(new BorderLayout());
        zobrazitTymyJButton.setText("Zobrazit seznam týmů");
        zobrazitTymyJButton.setBounds(new Rectangle(10, 200, 150, 20));
        zobrazitTymyJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    zobrazitTymyJButton_actionPerformed(e);
                }
            });
        zapasyJTable.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        zapasyJTable.setShowVerticalLines(false);
        tabulkaJTable.setShowVerticalLines(false);
        jTabbedPane.setBounds(new Rectangle(10, 40, 495, 315));

        jTabbedPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jTabbedPane.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    jTabbedPane_stateChanged(e);
                }
            });
        tabulkaJScrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0,
                                                                     0));
        tabulkaJScrollPane.setBackground(Color.white);
        informaceRozpisJPanel.setLayout(null);
        informaceRozpisJPanel.setPreferredSize(new Dimension(485, 198));
        informaceRozpisJPanel.setBorder(BorderFactory.createEmptyBorder(0, 0,
                                                                        0, 0));

        jLabelPopis_InfoOTymech.setText("Informace o týmech:");
        jLabelPopis_InfoOTymech.setForeground(new Color(0, 0, 82));
        jLabelPopis_InfoOTymech.setBounds(new Rectangle(10, 150, 100, 25));
        jLabelPopis_InfoNastaveniKola.setText("Volba kola:");
        jLabelPopis_InfoNastaveniKola.setForeground(new Color(0, 0, 82));
        volbaKolaJPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY,
                                                                 1));
        koloInnerJScrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0,
                                                                       0));
        ulozTabulkuJButton.setText("Uložit tabulku HTML");
        ulozTabulkuJButton.setBounds(new Rectangle(10, 375, 130, 20));
        ulozTabulkuJButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ulozTabulkuJButton_actionPerformed(e);
                }
            });
        jSeparator6.setForeground(new Color(214, 214, 214));
        jSeparator6.setBounds(new Rectangle(0, 365, 515, 10));
        volbaKolaJPanel.add(jLabelPopis_InfoNastaveniKola, null);
        volbaKolaJPanel.add(jLabelPopis_PeriodaVyber, null);
        volbaKolaJPanel.add(periodyJComboBox, null);
        volbaKolaJPanel.add(jLabelPopis_KoloVyber, null);
        volbaKolaJPanel.add(kolaJComboBox, null);
        // 2.tab - rozpis
        koloInnerJScrollPane.getViewport().add(zapasyJTable);
        koloJPanel.add(volbaKolaJPanel, BorderLayout.NORTH);
        koloJPanel.add(koloInnerJScrollPane, BorderLayout.CENTER);
        tabulkaJScrollPane.getViewport().add(tabulkaJTable,
                                             BorderLayout.CENTER);
        informaceRozpisJPanel.add(jLabelPopis_InfoOTymech, null);
        informaceRozpisJPanel.add(zobrazitTymyJButton, null);
        informaceRozpisJPanel.add(celkemZapasuJLabel, null);
        informaceRozpisJPanel.add(pocetTymuLabel, null);
        informaceRozpisJPanel.add(jLabelPopis_PocetTymu, null);
        informaceRozpisJPanel.add(jLabelPopis_CelkemZapasu, null);
        informaceRozpisJPanel.add(pocetPeriodLabel, null);
        informaceRozpisJPanel.add(pocetKolPeriodyLabel, null);
        informaceRozpisJPanel.add(pocetZapasuKolaLabel, null);
        informaceRozpisJPanel.add(jLabelPopis_PocetZapasuKola, null);
        informaceRozpisJPanel.add(jLabelPopis_PocetKolPeriody, null);
        informaceRozpisJPanel.add(jLabelPopis_PocetPeriod, null);
        informaceRozpisJPanel.add(jLabelPopis_InfoORozpisu, null);
        jTabbedPane.addTab("O rozpisu", informaceRozpisJPanel);
        jTabbedPane.addTab("Rozpis (výsledky)", koloJPanel);
        jTabbedPane.addTab("Tabulka", tabulkaJScrollPane);
        this.add(jTabbedPane, null);
        this.add(ulozTabulkuJButton, null);
        this.add(jSeparator6, null);
        this.add(jSeparator5, null);
        this.add(jLabelPopis_InfoSoubor, null);
        this.add(xmlJButton, null);
        this.add(nahledJButton, null);
        this.add(htmlJButton, null);
        this.add(jmenoSouboruJLabel, null);
        this.add(vybratJButton, null);
        myInit();
    }

    private void myInit() {
        ulozTabulkuJButton.setToolTipText("Uložíte tabulku vypočtenou z vašeho rozpisu do HTML souboru, který si můžete poté vytisknout");
        xmlJButton.setToolTipText("Uložení rozpisu v XML, tento soubor si poté můžete jindy otevřít a znovu s ním pracovat jako teď");
        htmlJButton.setToolTipText("Uložení rozpisu zápasů do HTML souboru, který si poté můžete vytisknout");
        nahledJButton.setToolTipText("Zobrazení náhledu celého rozpisu (všechny periody i kola)");
        nastavGUI(false);
        zapasyJTable.getModel().addTableModelListener(new TableModelListener() {
                public void tableChanged(TableModelEvent e) {
                    // model se zmeni, ikdyz se zmeni kolo (prepnuti comboboxem), proto toto osetreni, 
                    // aby k zapsani vysledku do pameti nedoslo, kdyz se zmeni vsechyn sloupce
                    if (e.getColumn() != e.ALL_COLUMNS) {
                        if (!zapasyJTable.getSkore().isEmpty()) {
                            xmlSoubor.nastavVysledek(zapasyJTable.getRadek(),
                                                     zapasyJTable.getSloupec(),
                                                     zapasyJTable.getSkore());
                        }
                    }
                }
            });
    }

    // NACTENI XML SOUBORU

    private void vybratJButotn_actionPerformed(ActionEvent e) {
        nactiSoubor();
    }

    private void nactiSoubor() {
        JFileChooser jf = new JFileChooser(".");
        FileNameExtensionFilter filtry =
            new FileNameExtensionFilter("xml", "XML");
        jf.setFileFilter(filtry);
        int result = jf.showDialog(this, "Otevřít XML soubor");
        if (result == JFileChooser.APPROVE_OPTION) {
            File f = jf.getSelectedFile();
            String adresa = f.toURI().getPath();
            String jmenoSouboru = f.getName();
            if (DOMVerificator.overSouborXML(adresa)) {
                try {
                    xmlSoubor = new RozpisXML(adresa);
                    jmenoSouboruJLabel.setText(jmenoSouboru);
                    aktualizujInformace();
                    aktualizujComboBoxy();
                    nastavGUI(true);
                    vypisAktualniKolo();
                    aktualizujTabulku();
                } catch (JDOMException e) {
                    showErrorPanel("JDOMException", e.getMessage());
                } catch (IOException e) {
                    showErrorPanel("IOException", e.getMessage());
                }
            }
        }
    }

    // VYGENEROVANI HTML SOUBORU S ROZPISEM

    private void htmlJButton_actionPerformed(ActionEvent e) {
        Exception f = xmlSoubor.exportHTML();
        if (f == null) {
            showInfoPanel("Export do HTML",
                          Export.Hlasky.ulozeniHTML(xmlSoubor.getHtmlAdresa()));
        } else {
            showErrorPanel("Chyba při exportu do HTML", f.getMessage());
        }
    }

    //  EXPORT XML SOUBORU

    private void xmlJButton_actionPerformed(ActionEvent e) {
        Exception f = xmlSoubor.exportXML();
        if (f == null) {
            showInfoPanel("Export do XML",
                          Export.Hlasky.ulozeniXML(xmlSoubor.getXmlAdresa()));
        } else {
            showErrorPanel("Chyba při exportu do XML", f.getMessage());
        }
    }

    // ULOZENI TABULKY DO HTML

    private void ulozTabulkuJButton_actionPerformed(ActionEvent e) {
        Exception f = xmlSoubor.exportTabulkaHTML();
        if (f == null) {
            showInfoPanel("Export do XML",
                          Export.Hlasky.ulozeniTabulkaHtml(xmlSoubor.getHtmlAdresaTabulky()));
        } else {
            showErrorPanel("Chyba při exportu do XML", f.getMessage());
        }
    }

    // NAHLED (Jtable) - nejde editovat, vypis vsech kol i period

    private void nahledJButton_actionPerformed(ActionEvent e) {
        NahledJPanel nahled = new NahledJPanel();
        nahled.setData(xmlSoubor.getRozpis());
        showInfoPanel("Náhled rozpisu", nahled);
    }

    // AKTUALIZACE LABELU PO NACTENI SOUBORU

    private void aktualizujInformace() {
        pocetPeriodLabel.setText("" + xmlSoubor.getPocetPeriod());
        pocetKolPeriodyLabel.setText("" + xmlSoubor.getPocetKolPeriody());
        pocetZapasuKolaLabel.setText("" + xmlSoubor.getPocetZapasuKola());
        pocetTymuLabel.setText("" + xmlSoubor.getPocetTymu());
        celkemZapasuJLabel.setText("" + xmlSoubor.getCelkemZapasu());
    }

    // COMBOBOXY

    private void aktualizujComboBoxy() {
        aktulizujPeriodyComboBox();
        aktualizujKolaComboBox();
        vypisAktualniKolo();
    }

    // PeriodyComboBox - 1 až početPeriod

    private void aktulizujPeriodyComboBox() {
        int pocetPeriod = xmlSoubor.getPocetPeriod();
        periodyJComboBox.removeAllItems();
        for (int i = 1; i <= pocetPeriod; i++) {
            periodyJComboBox.addItem(i);
        }
    }

    // KolaComboBox - počet = počet zápasů kola, číslování závislé na periodě

    private void aktualizujKolaComboBox() {
        int pocetKol = xmlSoubor.getPocetKolPeriody();
        int aktualniPerioda = periodyJComboBox.getSelectedIndex();
        kolaJComboBox.removeAllItems();
        for (int i = 1; i <= pocetKol; i++) {
            kolaJComboBox.addItem(aktualniPerioda * pocetKol + i);
        }
    }

    // AKCE U KolaComboBox -> vypsat nové kolo

    private void kolaJComboBox_actionPerformed(ActionEvent e) {
        vypisAktualniKolo();
    }

    // AKCE U PeriodyComboBox -> přepsat čísla kol u KolCB a vypsat nové kolo

    private void periodyJComboBox_actionPerformed(ActionEvent e) {
        aktualizujKolaComboBox();
        vypisAktualniKolo();
    }

    // VYPSANI AKTUALNIHO KOLA

    private void vypisAktualniKolo() {
        if (jdeVypsatTabulka()) {
            // zacinam od 0, ale ted to nevadi, protoze pak stejne get() zacina od 0
            int cisloPeriody = periodyJComboBox.getSelectedIndex();
            int cisloKola = kolaJComboBox.getSelectedIndex();
            xmlSoubor.nastavAktualniKolo(cisloPeriody, cisloKola);
            zapasyJTable.setData(xmlSoubor.getZapasyAktKola());
        }
    }

    private void aktualizujTabulku() {
        tabulkaJTable.setData(xmlSoubor.getVysledkovaTabulka());
    }

    // AKCE U ZobrazitTýmy BUTTON

    private void zobrazitTymyJButton_actionPerformed(ActionEvent e) {
        showInfoPanel("Seznam týmů", getSeznamTymu());
    }

    //  POMOCNE METODY

    private JScrollPane getSeznamTymu() {
        JScrollPane sp = new JScrollPane();
        sp.setPreferredSize(new Dimension(120, 200));
        sp.setBorder(null);
        JList tymy = new JList(xmlSoubor.getTymy());
        tymy.setBackground(SystemColor.control);
        sp.getViewport().add(tymy, null);
        return sp;
    }

    private boolean jdeVypsatTabulka() {
        return (periodyJComboBox.getItemCount() > 0) &&
            (kolaJComboBox.getItemCount() > 0);
    }

    private void showInfoPanel(String title, Object zprava) {
        JOptionPane.showMessageDialog(this, zprava, title,
                                      JOptionPane.PLAIN_MESSAGE);
    }

    private void showErrorPanel(String title, Object zprava) {
        JOptionPane.showMessageDialog(this, zprava, title,
                                      JOptionPane.ERROR_MESSAGE);
    }

    private void nastavGUI(boolean stav) {
        if (xmlSoubor == null) {
            jmenoSouboruJLabel.setText("Nebyl načten žádný soubor");
            vybratJButton.setText("Vybrat soubor");
        } else {
            vybratJButton.setText("Změnit soubor");
        }
        nahledJButton.setEnabled(stav);
        htmlJButton.setEnabled(stav);
        ulozTabulkuJButton.setEnabled(stav);
        xmlJButton.setEnabled(stav);
        zobrazitTymyJButton.setEnabled(stav);
        periodyJComboBox.setEnabled(stav);
        kolaJComboBox.setEnabled(stav);
        koloJPanel.setEnabled(stav);
        //jTabbedPane.setEnabled(stav);
    }


    private void jTabbedPane_stateChanged(ChangeEvent e) {
        if (jTabbedPane.getSelectedIndex() == 2) {
            if (xmlSoubor != null) {
                aktualizujTabulku();
            }
        }
    }
}
