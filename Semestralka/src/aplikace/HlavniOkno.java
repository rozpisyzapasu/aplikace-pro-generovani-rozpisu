package aplikace;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.UIManager;


public class HlavniOkno extends JFrame {

    private Container pane;
    private CardLayout layout;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        } catch (NoClassDefFoundError e) {
            e.printStackTrace();
        }
        new HlavniOkno();
    }

    private JMenuBar menuBar = new JMenuBar();
    private JMenu menuFile = new JMenu();
    private JMenuItem menuFileUpravy = new JMenuItem();
    private JMenuItem menuFileGenerovani = new JMenuItem();
    private JMenuItem menuFileExit = new JMenuItem();
    private JMenu menuHelp = new JMenu();
    private JMenuItem menuHelpAbout = new JMenuItem();
    private JMenuItem menuHelpHelp = new JMenuItem();


    public HlavniOkno() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setSize(new Dimension(524, 450));
        this.setResizable(false);
        this.setForeground(new Color(231, 231, 231));
        // nastaveni MENU
        this.setJMenuBar(menuBar);
        menuFile.setText("Menu");
        menuFileGenerovani.setText("Vygenerovat rozpis");
        menuFileGenerovani.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    layout.show(pane, "Generovani");
                }
            });
        menuFileUpravy.setText("Upravit rozpis");
        menuFileUpravy.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    layout.show(pane, "Upravy");
                }
            });
        menuFileExit.setText("Konec programu");
        menuFileExit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    fileExit_ActionPerformed(ae);
                }
            });
        menuHelp.setText("Nápověda");
        menuHelpHelp.setText("Průvodce");
        menuHelpHelp.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    helpHelp_ActionPerformed(ae);
                }
            });
        menuHelpAbout.setText("O programu");
        menuHelpAbout.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    helpAbout_ActionPerformed(ae);
                }
            });
        menuFile.add(menuFileGenerovani);
        menuFile.add(menuFileUpravy);
        menuFile.addSeparator();
        menuFile.add(menuFileExit);
        menuBar.add(menuFile);
        menuHelp.add(menuHelpHelp);
        menuHelp.add(menuHelpAbout);
        menuBar.add(menuHelp);
        // FRAME
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = this.getSize();
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }
        this.setLocation((screenSize.width - frameSize.width) / 2,
                         (screenSize.height - frameSize.height) / 2);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setTitle("Generátor rozpisů - www.online-generator-rozpisu.g6.cz");
        // nastaveni cardLayoutu
        layout = new CardLayout();
        setLayout(layout);
        pane = this.getContentPane();
        pane.add("Uvod", new Menu_Uvod());
        pane.add("Generovani", new Menu_Generator());
        pane.add("Upravy", new Menu_Upravy());
        layout.show(pane, "Uvod");
    }

    /**
     * menu -> konec programu
     */
    void fileExit_ActionPerformed(ActionEvent e) {
        System.exit(0);
    }

    /**
     * menu -> o programu
     */
    void helpAbout_ActionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, new Napoveda_OProgramu(),
                                      "O programu", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * menu -> pr�vodce
     */
    void helpHelp_ActionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, new Napoveda_Pruvodce(),
                                      "Průvodce", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * vnitrni trida pro uvodni obrazovku, abych mohl prepnout na obrazovku
     * generovani, prip. nacteni souboru
     */
    private class Menu_Uvod extends JPanel {
        private BorderLayout borderLayout1 = new BorderLayout();
        private JPanel jPanel1 = new JPanel();
        private JButton genJButton = new JButton();
        private JButton loadJButton = new JButton();
        private JPanel jPanel2 = new JPanel();
        private JTextPane jTextPane1 = new JTextPane();

        public Menu_Uvod() {
            try {
                jbInit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void jbInit() throws Exception {

            this.setLayout(borderLayout1);
            jPanel1.add(genJButton, null);
            jPanel1.add(loadJButton, null);
            this.add(jPanel1, BorderLayout.SOUTH);
            jPanel2.add(jTextPane1, null);
            this.add(jPanel2, BorderLayout.CENTER);
            genJButton.setText("Vygenerovat nový rozpis");
            genJButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        genJButton_actionPerformed(e);
                    }
                });
            loadJButton.setText("Upravit rozpis");
            loadJButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        loadJButton_actionPerformed(e);
                    }
                });
            jPanel2.setLayout(null);
            jPanel2.setBackground(Color.WHITE);
            jTextPane1.setBounds(new Rectangle(125, 100, 315, 125));
            jTextPane1.setText(getText());
            jTextPane1.setEditable(false);
            jTextPane1.setBackground(Color.WHITE);
        }


        private String getText() {
            String t = "";
            t += "VÍTEJTE V APLIKACI PRO GENEROVÁNÍ ROZPISU \n";
            t += "1. vygenerovat nový rozpis \n";
            t += "2. vkládat výsledky do vygenerovaného rozpisu \n\n";
            t += "Nápovědu k používání najdete v Průvodci\n";
            t += "Informace o samotném programu v sekci O programu \n";
            return t;
        }

        private void loadJButton_actionPerformed(ActionEvent e) {
            try {
                layout.show(pane, "Upravy");
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, e1.getLocalizedMessage());
                e1.printStackTrace();
            }
        }

        private void genJButton_actionPerformed(ActionEvent e) {
            layout.show(pane, "Generovani");
        }
    }

}
