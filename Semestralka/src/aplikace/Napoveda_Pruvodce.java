package aplikace;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;

public class Napoveda_Pruvodce extends JPanel {

    private JTabbedPane jTabbedPane1 = new JTabbedPane();
    private GridLayout gridLayout1 = new GridLayout();
    private JTextPane jTextPane1 = new JTextPane();
    private JTextPane jTextPane2 = new JTextPane();
    private JTextPane jTextPane3 = new JTextPane();

    public Napoveda_Pruvodce() {
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(gridLayout1);
        jTabbedPane1.setMinimumSize(new Dimension(400, 270));
        jTabbedPane1.setPreferredSize(new Dimension(400, 270));
        jTabbedPane1.setAutoscrolls(true);
        jTabbedPane1.addTab("Generování rozpisu", jTextPane1);
        jTabbedPane1.addTab("Úpravy rozpisu", jTextPane2);
        jTabbedPane1.addTab("Další informace", jTextPane3);
        jTextPane1.setText(getTextGenerator());
        jTextPane2.setText(getTextUpravy());
        jTextPane3.setText(getTextDalsiInfo());
        jTextPane1.setEditable(false);
        jTextPane2.setEditable(false);
        jTextPane3.setEditable(false);
        this.add(jTabbedPane1, null);
    }

    private String getTextGenerator() {
        String t = "";
        t += "1. počet týmů = netřeba vysvětlovat \n";
        t += "2. počet period = kolikrát se utká každý tým s každým (2-periody -> doma a venku atd.) \n";
        t += "3. počet zápasů kola = pro případ, že máte více týmů a méně hracích ploch než by bylo třeba, aby všechny týmy hrály současně \n";
        return t;
    }
    
    private String getTextUpravy() {
        String t = "";
        t += "- můžete vkládat výsledky zápasů ze kterých se následně vypočítá tabulka \n";
        t += "- tabulku i aktualizované rozpisy můžete exportovat do html  \n";
        t += "- !!! soubor název_vašeho_rozpisu.xml se při změnách automaticky neukládá \n";
        t += "- !!! výpočet tabulky skončí, jestliže v předchozím kole nebyl vyplněn žádný výsledek, např." +
             "máte-li vyplněná kola 1.,2. a 5., tak se do tabulky započítají jen první dvě kola (páté kolo" +             
            " se započítá až bude vyplněn aspoň jeden zápas ve 3. a 4. kole)\n";
        t += "- řazení tabulky: body - zápasy - vstřelené góly - obdržené góly";
        return t;
    } 
  
    private String getTextDalsiInfo() {
        return "Najdete na webu www.online-generator-rozpisu.g6.cz";
    }
}