package tabulka;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import javax.swing.table.AbstractTableModel;

public class TableModelRozpis extends AbstractTableModel {

    private String[] columnNames;
    private Object[][] data;

    public TableModelRozpis(String[] columnNames) {
        this.columnNames = columnNames;
        data = new String[0][columnNames.length];
    }

    public void setData(Object[][] noveZapasy) {
        data = noveZapasy;
        fireTableDataChanged();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 1 || col == 2) {
            return true;
        } else {
            return false;
        }
    }

    public void setValueAt(String value, int row, int col) {
        data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
}
