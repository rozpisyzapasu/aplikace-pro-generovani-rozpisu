package tabulka;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;

public class NahledJPanel extends JScrollPane {

    private RozpisJTable tabulka;

    public NahledJPanel() {
        super();
        try {
            tabulka = new RozpisJTable(false);
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.getViewport().add(tabulka, BorderLayout.CENTER);
    }

    public void setData(Object[][] obsah) {
        tabulka.setData(obsah);
    }
}
