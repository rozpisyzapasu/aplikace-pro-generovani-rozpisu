package tabulka;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

public class TabulkaJTable extends JTable {

    private TableModelRozpis model;

    public TabulkaJTable() {
        super();
        //obecne nastaveni
        nastavTabulku();
        nastavHlavicku();
        // specificke nastaveni
        nastavModel();
        nastavSirkySloupcu();
    }

    public void setData(Object[][] noveZapasy) {
        model.setData(noveZapasy);
    }

    private void nastavModel() {
        model =
                new TableModelRozpis(new String[] { "", "Tým", "Z", "V",
                                                    "R", "P", "VG",
                                                    "OG", "Body" });
        this.setModel(model);

        // nastaveni sirsiho prvniho sloupce
        TableColumn sloupec = this.getColumnModel().getColumn(1);
        sloupec.setPreferredWidth(200);
    }

    private void nastavSirkySloupcu() {
        // nastaveni sirsiho prvniho sloupce
        for (int i = 0; i < this.getColumnCount(); i++) {
            if (i != 1) {
                this.getColumnModel().getColumn(i).setPreferredWidth(30);
            }
        }
    }


    private void nastavTabulku() {
        this.setAutoCreateRowSorter(true);
        // okraje
        this.setBorder(null);
        this.setGridColor(Color.LIGHT_GRAY);
        // vycentorniva
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        this.setDefaultRenderer(Object.class, dtcr);        
    }

    private void nastavHlavicku() {
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        dtcr.setForeground(Color.DARK_GRAY);
        dtcr.setBackground(Color.LIGHT_GRAY);
        dtcr.setToolTipText("Kliknutím na položku z hlavičky můžete tabulku třídit");
        this.getTableHeader().setDefaultRenderer(dtcr);
        this.getTableHeader().setReorderingAllowed(false);
        this.getTableHeader().setResizingAllowed(false);
    }
}
