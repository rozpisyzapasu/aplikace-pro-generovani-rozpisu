package tabulka;

/*
 * This file is part of the GENERÁTOR ROZPISŮ
 * Copyright (c) 2011 Zdenek Drahos (https://bitbucket.org/zdenekdrahos)
 * GENERÁTOR ROZPISŮ is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License 3, or any later version
 * For the full license information view the file license.txt, or <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

public class RozpisJTable extends JTable {

    private TableModelRozpis model;
    private Bunka aktualniBunka;

    public RozpisJTable(boolean editable) {
        super();
        //obecne nastaveni
        nastavTabulku();
        nastavHlavicku();
        // specificke nastaveni - jestli rozpis nebo tabulka vysledku
        nastavJTableNaRozpis(editable);
    }      

    public void setData(Object[][] noveZapasy) {
        model.setData(noveZapasy);
    }

    private void nastavJTableNaRozpis(boolean editable) {
        model =
                new TableModelRozpis(new String[] { "Domácí", "Skóre domácí", "Skóre hosté",
                                                    "Hosté" });
        this.setModel(model);
        if (editable) {
            nastavEditovanySloupec(this.getColumnModel().getColumn(1));
            nastavEditovanySloupec(this.getColumnModel().getColumn(2));
        }
    }

    private void nastavTabulku() {
        aktualniBunka = new Bunka(0, 0, "");
        // okraje
        this.setGridColor(Color.LIGHT_GRAY);
        this.setBorder(new LineBorder(Color.LIGHT_GRAY, 1));
        this.setShowVerticalLines(false);
        // vycentorniva
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        this.setDefaultRenderer(Object.class, dtcr);        
    }

    private void nastavHlavicku() {
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        dtcr.setForeground(Color.DARK_GRAY);
        dtcr.setBackground(Color.LIGHT_GRAY);
        this.getTableHeader().setDefaultRenderer(dtcr);
        this.getTableHeader().setReorderingAllowed(false);
        this.getTableHeader().setResizingAllowed(false);
    }

    private void nastavEditovanySloupec(TableColumn sloupec) {
        sloupec.setCellEditor(new DefaultCellEditor(getComboBox()));
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(SwingConstants.CENTER);
        renderer.setForeground(Color.RED);
        renderer.setToolTipText("Klikněte pro zadání výsledku");
        sloupec.setCellRenderer(renderer);        
    }

    private JComboBox getComboBox() {
        JComboBox comboBox = new JComboBox();
        for (int i = 0; i < 100; i++) {
            comboBox.addItem(i);
        }
        comboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JComboBox cb = (JComboBox)e.getSource();
                    if (!cb.getSelectedItem().equals(-1)) {
                        if (!cb.getSelectedItem().equals("-")) {
                            nastavBunku(cb.getSelectedItem().toString());
                            nastavGoly();                            
                        }
                    }
                    // nastavit, aby se pri dalsim otevreni zacalo od 0
                    cb.setSelectedIndex(0);
                }
            });
        return comboBox;
    }

    public void nastavBunku(String hodnota) {
        if (model.getRowCount() > 0) {
            aktualniBunka.setBunka(getSelectedRow(), getSelectedColumn(),
                                   hodnota);
        }
    }

    private boolean lzeZapsatDoTabulky() {
        if (model.isCellEditable(aktualniBunka.radek, aktualniBunka.sloupec)) {
            String puvodni =
                (String)model.getValueAt(aktualniBunka.radek, aktualniBunka.sloupec);
            return !aktualniBunka.hodnota.equals(puvodni);
        }
        return false;
    }

    public void nastavGoly() {
        if (lzeZapsatDoTabulky()) {
            model.setValueAt(aktualniBunka.hodnota, aktualniBunka.radek,
                             aktualniBunka.sloupec);
        }
    }

    public int getSloupec() {
        return aktualniBunka.sloupec;
    }

    public int getRadek() {
        return aktualniBunka.radek;
    }

    public String getSkore() {
        return aktualniBunka.hodnota;
    }

    private class Bunka {
        private int radek;
        private int sloupec;
        private String hodnota;

        public Bunka(int radek, int sloupec, String hodnota) {
            this.radek = radek;
            this.sloupec = sloupec;
            this.hodnota = hodnota;
        }

        public void setBunka(int radek, int sloupec, String hodnota) {
            this.radek = radek;
            this.sloupec = sloupec;
            this.hodnota = hodnota;
        }

        public boolean jdeZmenitBunku(int radek, int sloupec, String hodnota) {
            boolean r = this.radek == radek;
            boolean s = this.sloupec == sloupec;
            boolean h = !this.hodnota.equals(hodnota);
            return r && s && h;
        }

        @Override
        public String toString() {
            return "[" + radek + ", " + sloupec + "] = " + hodnota;
        }
    }
}
